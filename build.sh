#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

readonly pfx="$PWD/local"
mkdir -p "$pfx"

# build avp
#
pushd "source"
mkdir -p build
cd build
cmake \
        -DCMAKE_PREFIX_PATH="$pfx" \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
        ..
make -j "$(nproc)"
popd

cp -rfv "source/build/avp" "3730/dist/avp"
cp -rfv "run-avp.sh" "3730/dist"

pushd "sdlcl"
make
popd

cp -rfv "sdlcl/libSDL-1.2.so.0" "3730/dist/"
